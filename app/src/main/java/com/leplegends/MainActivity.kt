package com.leplegends

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import com.leplegends.R.drawable.*
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private var currencyLeprechaunCoin = (8..19).random()
    private var currencyPlayerCoin = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        gameProcess()
        coinsAction()
        newGame()
    }

    private fun gameProcess() {
        val coins =
            arrayOf(iv_coin1, iv_coin2, iv_coin3, iv_coin4, iv_coin5, iv_coin6, iv_coin7)

        tv_amountCoins.text = currencyLeprechaunCoin.toString()
        tv_playerCurrencyCoin.text = currencyPlayerCoin.toString()

        cl_container.setOnClickListener {
            val a = (0..6).random()
            val marginBottom = (20..150).random()
            if (currencyLeprechaunCoin == 0) {
                cl_restart.visibility = View.VISIBLE
            } else {
                if (!coins[a].isVisible) {
                    currencyLeprechaunCoin -= 1
                    tv_amountCoins.text = currencyLeprechaunCoin.toString()
                    iv_leprechaun.animate().apply {
                        duration = 75
                        scaleX(0.98f)
                        scaleY(0.98f)
                    }.withEndAction {
                        iv_leprechaun.animate().apply {
                            duration = 75
                            scaleX(1.00f)
                            scaleY(1.00f)
                            changeVisibleCoin(coins, a, marginBottom)
                        }.start()
                    }
                }
            }

        }
    }

    private fun newGame() {
        val leprechaun = arrayOf(grandfather, grandfather1, grandfather2, grandfather3)

        btn_restart.setOnClickListener {
            iv_leprechaun.setImageResource(leprechaun.random())
            currencyLeprechaunCoin = (8..19).random()
            tv_amountCoins.text = currencyLeprechaunCoin.toString()
            cl_restart.visibility = View.GONE
            iv_coin1.visibility = View.INVISIBLE
            iv_coin2.visibility = View.INVISIBLE
            iv_coin3.visibility = View.INVISIBLE
            iv_coin4.visibility = View.INVISIBLE
            iv_coin5.visibility = View.INVISIBLE
            iv_coin6.visibility = View.INVISIBLE
            iv_coin7.visibility = View.INVISIBLE
            iv_coin8.visibility = View.INVISIBLE
        }
    }

    private fun changeVisibleCoin(
        coins: Array<ImageView>,
        a: Int,
        marginBottom: Int
    ) {
        val layoutParams = coins[a].layoutParams as ConstraintLayout.LayoutParams
        layoutParams.bottomMargin = marginBottom
        coins[a].visibility = View.VISIBLE
        coins[a].layoutParams = layoutParams
    }

    private fun changePlayerCurrency() {
        currencyPlayerCoin += 1
        tv_playerCurrencyCoin.text = currencyPlayerCoin.toString()
    }

    private fun coinsAction() {
        iv_coin1.setOnClickListener {
            if (iv_coin1.isVisible) {
                iv_coin1.visibility = View.INVISIBLE
                changePlayerCurrency()
            }
        }

        iv_coin2.setOnClickListener {
            if (iv_coin2.isVisible) {
                iv_coin2.visibility = View.INVISIBLE
                changePlayerCurrency()
            }
        }

        iv_coin3.setOnClickListener {
            if (iv_coin3.isVisible) {
                iv_coin3.visibility = View.INVISIBLE
                changePlayerCurrency()
            }
        }

        iv_coin4.setOnClickListener {
            if (iv_coin4.isVisible) {
                iv_coin4.visibility = View.INVISIBLE
                changePlayerCurrency()
            }
        }

        iv_coin5.setOnClickListener {
            if (iv_coin5.isVisible) {
                iv_coin5.visibility = View.INVISIBLE
                changePlayerCurrency()
            }
        }

        iv_coin6.setOnClickListener {
            if (iv_coin6.isVisible) {
                iv_coin6.visibility = View.INVISIBLE
                changePlayerCurrency()
            }
        }

        iv_coin7.setOnClickListener {
            if (iv_coin7.isVisible) {
                iv_coin7.visibility = View.INVISIBLE
                changePlayerCurrency()
            }
        }

        iv_coin8.setOnClickListener {
            if (iv_coin8.isVisible) {
                iv_coin8.visibility = View.INVISIBLE
                changePlayerCurrency()
            }
        }
    }
}